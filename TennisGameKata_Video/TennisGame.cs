﻿using System;
using System.Collections.Generic;

namespace TennisGameKata_Video
{
	public class TennisGame
	{
		private readonly string _firstPlayerName;
		private readonly string _secondPlayerName;
		private int _firstPlayerScoreTimes;//a
		private int _secondPlayerScoreTimes;//b

		public TennisGame(string firstPlayerName, string secondPlayerName)
		{
			_firstPlayerName = firstPlayerName;
			_secondPlayerName = secondPlayerName;
		}

		public void FirstPlayerScore()
		{
			_firstPlayerScoreTimes++;
		}

		public string Score()
		{
			#region win
			if (_firstPlayerScoreTimes >= 4 && Math.Abs(_firstPlayerScoreTimes - _secondPlayerScoreTimes) >= 2)
			{
				return _firstPlayerName + " Win";
			}
			else if (_secondPlayerScoreTimes >= 4 && Math.Abs(_firstPlayerScoreTimes - _secondPlayerScoreTimes) >= 2)
			{
				return _secondPlayerName + " Win";
			}
			#endregion

			#region Adv
			if (_firstPlayerScoreTimes > _secondPlayerScoreTimes && _firstPlayerScoreTimes > 3)
			{
				return _firstPlayerName + " Adv";
			}
			else if (_firstPlayerScoreTimes < _secondPlayerScoreTimes && _secondPlayerScoreTimes > 3)
			{
				return _secondPlayerName + " Adv";
			}
			#endregion

			#region Deuce and all
			if (_firstPlayerScoreTimes == _secondPlayerScoreTimes)
			{
				if (_firstPlayerScoreTimes < 3 && _secondPlayerScoreTimes < 3)
				{
					return ScoreToString(_firstPlayerScoreTimes)+ " All";
				}
				else{
					return "Deuce";
				}
			}
			#endregion
			
			//other
			return ScoreToString(_firstPlayerScoreTimes) + ' ' + ScoreToString(_secondPlayerScoreTimes);
		}

		public void SecondPlayerScore()
		{
			_secondPlayerScoreTimes++;
		}

		public string ScoreToString(int i) {
			switch (i) {
				case 0:
					return "Love";
				case 1:
					return "Fifteen";
				case 2:
					return "Thirty";
				case 3:
					return "Forty";
			}
			return "";
		}
	}
}